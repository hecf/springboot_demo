package org.demo.hecf.demo.user.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;

import java.util.Date;

/** 用户表信息
 * @author hcf
 * @email hcf0108@163.com
 * @create 2018-08-10 下午 12:49
 * 使用 lombok的get ，set 注解
 */
@Log
@Getter
@Setter
public class User {

    private String name;
    @JsonIgnore
    private String password;

    private Integer age;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss a",locale = "zh",timezone = "GMT+8")
    private Date birthday;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String desc;
}
