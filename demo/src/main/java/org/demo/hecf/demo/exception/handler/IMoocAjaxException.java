package org.demo.hecf.demo.exception.handler;

import org.demo.hecf.demo.jsonResult.IMoocJSONResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ajax异常捕获助手
 * @author hcf
 * @email hcf0108@163.com
 * @create 2018-08-21 下午 12:46
 **/
//@RestControllerAdvice
public class IMoocAjaxException {

//    @ExceptionHandler(value = Exception.class)
   public IMoocJSONResult defaultErrorHandler(HttpServletRequest request, HttpServletResponse response, Exception e )throws  Exception{
        e.printStackTrace();
        return IMoocJSONResult.errorException(e.getMessage());
    }

}
