package org.demo.hecf.demo.hello.controller;

import lombok.Getter;
import lombok.Setter;
import org.demo.hecf.demo.jsonResult.IMoocJSONResult;
import org.demo.hecf.demo.resource.props.Resource;
import org.demo.hecf.demo.user.pojo.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * hello的控制层
 * @author hcf
 * @email hcf0108@163.com
 * @create 2018-08-10 下午 12:42
 **/
@RestController//RestController = Controller + ResponseBody
public class HelloController {

    @Autowired
    @Getter
    @Setter
    private Resource resource;

    @RequestMapping("/hello")
    public Object hello(){
        return  "hello ! Spring boot~~!";
    }

    @RequestMapping("/getResource")
    public IMoocJSONResult getResource(){
        Resource bean = new Resource();
        BeanUtils.copyProperties(resource,bean);
//        return IMoocJSONResult.ok(resource);
        return IMoocJSONResult.ok(bean);
   }

}
