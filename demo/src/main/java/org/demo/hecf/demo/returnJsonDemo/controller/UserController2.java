package org.demo.hecf.demo.returnJsonDemo.controller;

import lombok.extern.java.Log;
import org.demo.hecf.demo.jsonResult.IMoocJSONResult;
import org.demo.hecf.demo.user.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author hcf
 * @email hcf0108@163.com
 * @create 2018-08-10 下午 1:09
 **/

@Log
@RestController
@RequestMapping("user2")
public class UserController2 {
    @RequestMapping("getUser")
    public User getUser(){
        User user = new User();
        user.setName("Imooc");
        user.setAge(18);
        user.setBirthday(new Date());
        user.setPassword("Imooc");
        log.info(user.getName());
        return  user;
    }

    @RequestMapping("getUserJson")
    public IMoocJSONResult getUserJson(){
        User user = new User();
        user.setName("Imooc");
        user.setAge(18);
        user.setBirthday(new Date());
        user.setPassword("imooc");

        return IMoocJSONResult.ok(user);
    }
}
