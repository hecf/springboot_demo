package org.demo.hecf.demo.exception.controller;

import org.demo.hecf.demo.jsonResult.IMoocJSONResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 异常演示Controller
 *
 * @author hcf
 * @email hcf0108@163.com
 * @create 2018-08-21 下午 12:14
 **/
@Controller
@RequestMapping("err")
public class ErrorController {

    @RequestMapping("error")
    public String error(){
        int a = 1/0 ;
        return "thymeleaf/error";
    }

    @RequestMapping("ajaxerror")
    public String ajaxerror(){
        return "thymeleaf/ajaxerror";
    }

    @RequestMapping("getAjaxerror")
    public IMoocJSONResult getAjaxerror(){
        int a = 1/0 ;
        return IMoocJSONResult.ok();
    }
}
