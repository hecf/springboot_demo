package org.demo.hecf.demo.freemarker.controller;

import lombok.Getter;
import lombok.Setter;
import org.demo.hecf.demo.resource.props.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * fremarker整合的controller演示
 *
 * @author hcf
 * @email hcf0108@163.com
 * @create 2018-08-16 下午 1:12
 **/
@Controller
@RequestMapping("ftl")
public class FreemarkerController {

    @Autowired
    @Getter
    @Setter
    private Resource resource;

    @RequestMapping("index")
    public String index(ModelMap map){
        map.addAttribute("resource",resource);
        return "freemarker/index";
    }

    @RequestMapping("center")
    public String center(ModelMap map){
        map.addAttribute("resource",resource);
        return "freemarker/center/center";
    }

}
