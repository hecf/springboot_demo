package org.demo.hecf.demo.exception.handler;

import org.demo.hecf.demo.jsonResult.IMoocJSONResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 统一页面异常助手
 * @author hcf
 * @email hcf0108@163.com
 * @create 2018-08-21 下午 12:37
 **/
@RestControllerAdvice//返回json
@ControllerAdvice//返回modelAndView
public class IMoocExceptionHandler {
    public static final String IMOOC_ERROR_VIEW="error";

//    @ExceptionHandler(value = Exception.class)
//    public Object errorHandler(HttpServletRequest request, HttpServletResponse response, Exception e) throws  Exception{
//        e.printStackTrace();
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.addObject("exception",e);
//        modelAndView.addObject("url", request.getRequestURL());
//        modelAndView.setViewName(IMoocExceptionHandler.IMOOC_ERROR_VIEW);
//        return  modelAndView;
//    }

    @ExceptionHandler(value = Exception.class)
    public Object errorHandle(HttpServletRequest request,HttpServletResponse response,Exception e){
        e.printStackTrace();
        if(isAjax(request)){
            return  IMoocJSONResult.errorException(e.getMessage());
        }else {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("exception",e);
            modelAndView.addObject("url", request.getRequestURL());
            modelAndView.setViewName(IMoocExceptionHandler.IMOOC_ERROR_VIEW);
            return  modelAndView;
        }

    }

    public boolean isAjax(HttpServletRequest request) {
        return request.getHeader("X-Requested-With")!=null
                && "XMLHttpRequest".equals( request.getHeader("X-Requested-With").toString() );

    }
}
