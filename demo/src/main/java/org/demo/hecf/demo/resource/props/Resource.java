package org.demo.hecf.demo.resource.props;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 资源文件实体类
 *
 * @author hcf
 * @email hcf0108@163.com
 * @create 2018-08-15 下午 1:05
 **/
@Configuration
@ConfigurationProperties(prefix = "com.imooc.opensource")
@PropertySource(value = "classpath:resource.properties")
public class Resource {
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String website;
    @Getter
    @Setter
    private String language;

}
