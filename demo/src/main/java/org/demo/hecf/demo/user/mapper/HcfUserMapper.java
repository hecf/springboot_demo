package org.demo.hecf.demo.user.mapper;

import org.demo.hecf.demo.user.pojo.HcfUser;
import org.demo.hecf.demo.util.MyMapper;

public interface HcfUserMapper extends MyMapper<HcfUser> {
}